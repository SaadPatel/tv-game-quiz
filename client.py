SERVER_IP = ''     #No need to mention anything in command line
SERVER_PORT = 5555 

import socket, threading, time, random, sys, select, termios

try: #For closing the socket properly without intervening with internal errors


    def receiveMessage(s,killRequest):# s is socket, killRequest is threading.Event object, used for inter-thread communication.
        
        while not killRequest.isSet(): #Waiting for buzzer to be pressed
            
            r, w, x = select.select([s], [], []) #select function implementation below
            
            data = r[0].recv(1024)
            
            print data
            
            if data:
                killRequest.set()

    def sendMessage(s, userid, killRequest, youBuzzed):
     
     while not killRequest.isSet(): #Buzzzeeerrrrrrrrrrrrrrrrr
     
            r, w, x = select.select([sys.stdin], [], [], 0.02)

            if r:#if any source gives readable data

                s.send(str(userid))

                youBuzzed.set()

                time.sleep(0.01)

                break

    serverip = SERVER_IP

    serverport = SERVER_PORT

    clientport = random.randint(2000, 3000)


    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    
    s.bind(('', clientport))
    
    s.connect((serverip, serverport))
    
    userid = s.recv(1024)
    
    print "Hello friend, Welcome to Kaun Banega programmer. You are player number", userid
    
    win_score = s.recv(1024)
    
    print "Please get ready to play, The game is ON.(Sherlock). If you know the answer and want to reserve the chance you have to press buzzer once buzzer is live.If you score", win_score, "You will win the game and program will end"

    #game loop
    continue_next_round = 1 
        
        question = s.recv(1024)
#Buzzer stuff
        killRequest = threading.Event()
        
        youBuzzed = threading.Event()

        sendThread = threading.Thread(target = sendMessage, args = [s, userid, killRequest, youBuzzed]) 
        
        receiveThread = threading.Thread(target = receiveMessage, args = [s, killRequest]) 

        time.sleep(0.1)
        
        print "Dear host, please enter an interesting question.....", question
        
        print "Buzzer is live!.... If you know answer then only press buzzer....."
        
        #termios.tcflush(sys.stdin, termios.TCIOFLUSH) #empty the sys.stin buffer as a precaution
        
        s.setblocking(0)#setblocking(0) makes all the socket methods non-blocking. This is actually to enable the select functionality on the server side.
        
        sendThread.start()
        
        receiveThread.start()
        
    
        receiveThread.join()
        
        sendThread.join()

        s.setblocking(1)#After the buzzer functionality is over, returning the sockets to a sequential processing mode
        
        termios.tcflush(sys.stdin, termios.TCIOFLUSH) 
        
        time.sleep(0.01)
        
        if youBuzzed.isSet():
            print "Please answer the question very fast....."
            
            givenAnswer = raw_input()
            
            s.send(givenAnswer)
        
        else:
        
            givenAnswer = s.recv(1024)
        
            print "You answered...:", givenAnswer
            
        is_correct_str = s.recv(1024)
        
        time.sleep(0.001)
        
        print is_correct_str
        
        trueAnswer = s.recv(1024)
        
        print "Broooo Correct answer is:",trueAnswer

        tally = s.recv(1024) 
        
        tally = tally.split()

        print "Score till now"
        
        for i in range(len(tally)):
        
            print i, tally[i]
        
        continue_next_round = s.recv(1024)
        
        continue_next_round = int(continue_next_round)

    final_message = s.recv(1024) 
    
    print final_message
except Exception as e:
    
    print e #printing the error message
finally: #Socket closing...... 
    
    s.close()
