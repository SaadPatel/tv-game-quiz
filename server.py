NUM_CLIENTS = 3
WIN_SCORE = 5
SERVER_PORT = 5555

import socket, select, time


#TCP socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

port = SERVER_PORT

s.bind(('',port))

s.listen(NUM_CLIENTS) #connections and clients equating

clients = [0]*NUM_CLIENTS# participant player numbers

for i in range(NUM_CLIENTS): 

    clients[i], addr = s.accept()

    clients[i].send(str(i))

    print "Connection to client",i,"established. Server working properly....Stage 1"


time.sleep(0.001) 

for i in range(NUM_CLIENTS):

    clients[i].send(str(WIN_SCORE))

tally = [0]*NUM_CLIENTS #scoreboard

time.sleep

while True: #game loop
    

    time.sleep(0.001)

    buzzed = -1 #playernumber of client who hits buzzer



    question = raw_input("The Question: ")

    trueAnswerStr = raw_input("The Answer: ")

    trueAnswer = (trueAnswerStr.lower()).split()#splits into an array of lowercase constituent words

    for s in clients:

        s.send(question)


    for s in clients:

        s.setblocking(0)

    while True:#buzzer listening mechanism




        r, w, x = select.select(clients, [], [])# r == the sources select monitors for reading into, w, x irrelevant for us

        s = r[0]

        buzzed = s.recv(1024)#when buzzing, the clients send their playernumber

        if buzzed:

            break
    
    buzzed = int(buzzed)

    print "Player", buzzed, "buzzed."


    for i in range(NUM_CLIENTS):#tell the other clients who buzzed

        s = clients[i]

        if i != buzzed:

            s.send(str(buzzed) + " buzzed....chill")

        else:
            s.send("You buzzed ..... so please answer fast....")
    
    for s in clients:

        s.setblocking(1)
    time.sleep(0.1)

    givenAnswer = (clients[buzzed]).recv(1024)

    print "Given answer: ", givenAnswer

    for i in range(NUM_CLIENTS):#tell the other clients the answer given by the active player

        if i != buzzed:

            clients[i].send(givenAnswer)

    time.sleep(0.01)



    givenAnswer = (givenAnswer.lower()).split()


    answeredCorrectly = True

    print trueAnswer, givenAnswer

    for i in trueAnswer:#if every word in the answer is in the given, correct. Else incorrect

        if not (i in givenAnswer):

            answeredCorrectly = False


            break

    if answeredCorrectly:

        print "Answered correctly.... Good Job.. See you in next round"

        for s in clients:

            s.send("Correctly answered.... Competition is increasing......")

        tally[buzzed]+=1

    else:

        print "Wrong answerrrrr...... You are wasting others time"

        for s in clients:

            s.send("Wrong answer.... Don't waste Buzzer plzzzzzzzzz!")



    tallyStr = '' #convert the tally array into a str to send across to the clients

    time.sleep(0.01)


    for i in tally:

        tallyStr += ' ' + str(i)

    for s in clients:

        s.send(trueAnswerStr)

        time.sleep(0.01)

        s.send(tallyStr)
        
    time.sleep(0.01)
    
    if(WIN_SCORE in tally):#Game stops if somebody wins

        for s in clients:

            s.send("0")

        break

    else:

        for s in clients:

            s.send("1")


time.sleep(0.01)

for i in range(NUM_CLIENTS):

    if tally[i] == WIN_SCORE:

        clients[i].send("Congratulations....You won!.... Here is a challenge for you 1)find a bug in socket function which may lead to two winners...")

    else:

        clients[i].send("You lose!...... Better luck next time")

#Any future plans.....




